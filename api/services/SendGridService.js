/**
 * Created by Saurabh on 04/08/17.
 */

const KEY = sails.config.styfi.EMAIL_KEY;
var sendGrid = require('sendgrid')(KEY);
var sendGridConfig = require('sendgrid').mail;

module.exports = {
    email: email
};

function email() {
    this.from = false;
    this.to = false;
    this.subject = false;
    this.content = false;
    this.contentType = false;
    this.mimeType = false;
    this.cc = false;
    this.bcc = false;
}


email.prototype.setValue = function(key, value){
    if(_.has(this, key)){
        if(_.isString(value))
            value = value.trim();
        this[key] = value;
    }
};

email.prototype.setValues = function(data){
    _.forEach(data, function(value, key){
        this.setValue(key, value);
    }.bind(this));
};


email.prototype.send = function (callback){
    if(this.from && this.content && this.subject && this.contentType && this.mimeType) {
        var mail = new sendGridConfig.Mail(new sendGridConfig.Email(this.from), this.subject, new sendGridConfig.Email(this.to), new sendGridConfig.Content(this.mimeType, this.content));
        var request = sendGrid.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });
        sendGrid.API(request, function (error, response) {
            if (error) {
                callback(error);
            } else {
                callback(null, response);
            }
        });
    } else {
        callback(true, null);
    }
};







