/**
 * Sms.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


var SMS_SENDER = sails.config.styfi.SMS_SENDER;
var SMS_USERNAME = sails.config.styfi.SMS_USERNAME;
var SMS_HASH_CODE = sails.config.styfi.SMS_HASH_CODE;
var SMS_URL = sails.config.styfi.SMS_URL;


module.exports =
    {

        attributes: {

        },

        send_sms:function (id,mobile_number,messages, cb) {
            //console.log(id);
            //console.log(mobile_number);
            //console.log(messages);

            var request = require('request');
            var msg = messages.split(' ').join('+');
            //console.log(msg);
            var toNumber = '91'+mobile_number;
            var username = SMS_USERNAME;
            var hash = SMS_HASH_CODE; // The hash key could be found under Help->All Documentation->Your hash key. Alternatively you can use your Textlocal password in plain text.
            var sender = SMS_SENDER;
            var test = id;
            var data = {
                "username": username,
                "hash": hash,
                "message": msg,
                "sender": sender,
                "numbers": toNumber,
                "test":test
            }//'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + toNumber + '&message=' + msg;
            var options = {
                method:'POST',
                uri: SMS_URL,
                form:data
            };
            //send request
            request(options, function(error,response,body){
                if (!error) {
                    cb(null,body);
                }
                else {
                    console.log('Error happened: '+ error);
                    cb(error);
                }

            });
        }
    };


