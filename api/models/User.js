/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var db_prefix = sails.config.styfi.db_prefix;
var db_sl_prefix = sails.config.styfi.db_sl_prefix;
var cdn = sails.config.styfi.cdn_url;
var cdn1 =sails.config.styfi.cdn_blank_url;
var s3 =sails.config.styfi.s3_url;
module.exports = {
  attributes: {
      username: {
          type: 'string',
          required: true
      },
      mobile_number: {
          type: 'string',
          required: true
      },
      email: {
          type: 'string',
          required: true
      }
  },

  /**
  * Check validness of a login using the provided inputs.
  * But encrypt the password first.
  *
  * @param  {Object}   inputs
  * @param  {Function} cb
  */

  attemptsignup: function (inputs, cb) {
      if (inputs.username && inputs.email && inputs.mobile_number && inputs.type && inputs.foreign_id) {
          User.query('SELECT * FROM sl_customer WHERE mobile_number = "' + inputs.mobile_number + '"', function (err, output) {
              //console.log(output.length);
              if (output.length > 0) {
                  return  cb({
                      'error' :true,
                      'code' :404,
                      'message' :'Mobile Number already exists!'
                    });
              }
              else {
                  var salt = "";
                  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                  for (var i = 0; i < 5; i++)
                      salt += possible.charAt(Math.floor(Math.random() * possible.length));
                  var unique_mob_number = sails.sha1(salt + sails.sha1(salt + sails.sha1(inputs.mobile_number)));

                  User.query('insert into sl_customer (foreign_id,type, username, mobile_number, email,unique_mob_number,salt, date_added,status) values ("' + inputs.foreign_id + '","' + inputs.type + '","' + inputs.username + '","' + inputs.mobile_number + '","' + inputs.email + '","' + unique_mob_number + '","' + salt + '",NOW(),1)', function (err, res) {
                      //console.log(res);
                      if (err) {
                          return cb({
                              "code":403,
                              "message":"Invalid Username/email/mobile_number"
                            });
                      }
                      else {
                          User.query('INSERT INTO sl_api_auth_token (token,customer_id,device_type,create_datetime,expire_time,logout_status) values (MD5(NOW()),' + res.insertId + ',"web",NOW(),NOW()+INTERVAL 15 DAY,0)', function (err, resp) {
                              if (err) {
                                  return  cb({
                                    "code":403,
                                    "message":"Invalid Username/email/mobile_number"
                                  });
                              }
                              else {
                                  User.query('SELECT * FROM sl_customer cu ,sl_api_auth_token at WHERE cu.customer_id= at.customer_id and at.api_token_id=' + resp.insertId, function (error, userData) {
                                      if (error) {
                                          return  cb({
                                              "code":403,
                                              "message":"Invalid Username/email/mobile_number"
                                            });
                                      }
                                      else {
                                          return  cb(false, userData);
                                      }
                                  });
                              }
                          });
                      }
                  });
              }
          });
      }
      else {
          return  cb({
              "code": 403,
              "message": "Required Parameters Missing"
          });
      }
  },


  attemptLogin: function (inputs, cb) {
        if (inputs.mobile_number) {
            User.query('SELECT *  FROM sl_customer WHERE mobile_number ="' + inputs.mobile_number + '"', function (err, rpl) {
                if (err) {
                    return cb({
                      "code":403,
                      "message":"invalid mobile_number"
                    });
                }
                if (rpl.length === 1) {
                            var salt = rpl[0].salt;
                            var unique_mob_number = sails.sha1(salt + sails.sha1(salt + sails.sha1(inputs.mobile_number)));
                            //console.log(salt);
                            //console.log(unique_mob_number);
                            User.query('SELECT * FROM sl_customer WHERE unique_mob_number ="' + unique_mob_number + '"', function (error, rawResult) {
                                //console.log(rawResult);
                                if (rawResult.length === 1) {
                                    //login success
                                    if(rawResult[0].type === "user"){
                                        User.query('INSERT INTO sl_admin_api_token (token,user_id,create_date,expires,status) values (MD5(NOW()),' + rawResult[0].foreign_id + ',NOW(),NOW()+INTERVAL 15 DAY,1)', function (err, res) {
                                            if (err) {
                                                return  cb({
                                                    "code":403,
                                                    "message":"invalid mobile_number"
                                                });
                                            }
                                            else {
                                                User.query('SELECT * FROM oc_user cu ,sl_admin_api_token at WHERE cu.user_id= at.user_id and at.admin_api_id=' + res.insertId, function (error, adminData) {

                                                    if (error) {
                                                        return  cb({
                                                            "code":403,
                                                            "message":"invalid mobile_number"
                                                        });
                                                    }
                                                    else {
                                                            User.query("select om.background_image,om.sign_image,os.is_styfi_fulfillable,om.status,om.name as brand_name,om.image as brand_image,om.cover_image,om.web_image,sc.type,ou.user_group_id,sc.foreign_id as user_id,os.has_sheet,ps.name,ps.store_type,ps.domain,ps.styfi_store_id as store_id,ps.brand_id,ps.is_enable ,apt.token as token\n" +
                                                                "from  sl_customer sc\n" +
                                                                "JOIN  oc_user ou ON ou.user_id= sc.foreign_id\n" +
                                                                "JOIN  sl_partner_store ps ON ps.styfi_store_id = ou.store_id\n" +
                                                                "JOIN  oc_store os ON ou.store_id = os.store_id\n" +
                                                                "JOIN  sl_admin_api_token apt ON apt.user_id = sc.foreign_id\n" +
                                                                "JOIN  oc_manufacturer om ON om.manufacturer_id=ps.brand_id \n" +
                                                                "where sc.foreign_id=" +adminData[0].user_id+" order by apt.admin_api_id desc limit 1",function (err,Data){
                                                                //console.log(Data);
                                                                if (err)
                                                                {
                                                                    return cb(err);
                                                                }else
                                                                {
                                                                    if(Data[0].status === 0)
                                                                    {
                                                                        return  cb({
                                                                            'error' :true,
                                                                            'code' :404,
                                                                            'message' :'Your account has not been activated yet.'
                                                                        });
                                                                    }else {
                                                                        //var x = Data[0].brand_name;
                                                                        //console.log(s3+x.charAt(0)+'-200x200.png');
                                                                        for(var i=0 ; i< Data.length; i++)
                                                                        {
                                                                            if (Data[i].brand_image)
                                                                            {
                                                                                Data[i].brand_image =cdn+Data[i].brand_image;
                                                                            }
                                                                            else
                                                                            {
                                                                                var x = Data[i].brand_name;
                                                                                Data[i].brand_image =s3+x.charAt(0)+'-200x200.png';
                                                                            }
                                                                            if(Data[i].background_image)
                                                                            {
                                                                                Data[i].background_image=cdn+Data[i].background_image;
                                                                            }
                                                                            if(Data[i].sign_image)
                                                                            {
                                                                                Data[i].sign_image=cdn+Data[i].sign_image;
                                                                            }
                                                                            if(Data[i].cover_image)
                                                                            {
                                                                                Data[i].cover_image=cdn+Data[i].cover_image;
                                                                            }
                                                                        }
                                                                        for(var j=0 ; j< Data.length; j++)
                                                                        {
                                                                            if(Data[j].cover_image ==='')
                                                                            {
                                                                                Data[j].cover_image =cdn1;
                                                                            }
                                                                            if(Data[j].web_image ==='')
                                                                            {
                                                                                Data[j].web_image =cdn1;
                                                                            }
                                                                            if(Data[j].background_image ===null)
                                                                            {
                                                                                Data[j].background_image =cdn1;
                                                                            }
                                                                            if(Data[j].sign_image ===null)
                                                                            {
                                                                                Data[j].sign_image =cdn1;
                                                                            }
                                                                        }
                                                                        return  cb(false, Data);

                                                                    }
                                                                }
                                                            });
                                                    }
                                                });
                                            }
                                        });
                                    }else if (rawResult[0].type === "customer")
                                    {
                                        User.query('INSERT INTO sl_api_auth_token (token,customer_id,device_type,create_datetime,expire_time,logout_status) values (MD5(NOW()),' + rawResult[0].foreign_id + ',"web",NOW(),NOW()+INTERVAL 15 DAY,1)', function (err, res) {
                                            if (err) {
                                                return  cb({
                                                    "code":403,
                                                    "message":"invalid mobile_number1"
                                                });
                                            }
                                            else {
                                                User.query('SELECT * FROM  sl_customer cu \n' +
                                                    'JOIN  sl_api_auth_token sat ON sat.customer_id=cu.foreign_id\n' +
                                                    'WHERE sat.api_token_id=' + res.insertId, function (error, adminData) {
                                                    //console.log(adminData);
                                                    if (error) {
                                                        return  cb({
                                                            "code":403,
                                                            "message":"invalid mobile_number2"
                                                        });
                                                    }
                                                    else {
                                                            User.query("select * from  sl_customer sc\n" +
                                                                "JOIN  oc_customer oc ON oc.customer_id = sc.foreign_id\n" +
                                                                "JOIN  sl_api_auth_token sat ON sat.customer_id=sc.foreign_id\n" +
                                                                "where sc.foreign_id=" +adminData[0].foreign_id+" order by sat.api_token_id desc limit 1",function (err,Data1){
                                                                //console.log(Data1);
                                                                if (err)
                                                                {
                                                                    return cb(err);
                                                                }else
                                                                {
                                                                    return  cb(false, Data1);
                                                                }
                                                            });
                                                    }
                                                });
                                            }
                                        });
                                    }else
                                    {
                                        User.query('INSERT INTO sl_admin_api_token (token,user_id,create_date,expires,status) values (MD5(NOW()),' + rawResult[0].foreign_id + ',NOW(),NOW()+INTERVAL 15 DAY,1)', function (err, res) {
                                            //console.log(res);
                                            if (err) {
                                                return  cb({
                                                    "code":403,
                                                    "message":"invalid mobile_number"
                                                });
                                            }
                                            else {
                                                User.query('SELECT * FROM oc_user cu ,sl_admin_api_token at WHERE cu.user_id= at.user_id and at.admin_api_id=' + res.insertId, function (error, adminData) {
                                                    // console.log(adminData[0].token);
                                                    if (error) {
                                                        return  cb({
                                                            "code":403,
                                                            "message":"invalid mobile_number"
                                                        });
                                                    }
                                                    else {

                                                               var Data =[ {
                                                                "type": "admin",
                                                                "user_id": adminData[0].user_id,
                                                                "name": 'styfi',
                                                                "brand_name": 'STYFI',
                                                                "brand_image": "https://s3-ap-southeast-1.amazonaws.com/styfi-image/cache/catalog/manufacturers/S-200x200.png",
                                                                "cover_image": "https://dpnexo4f8mouj.cloudfront.net/cache/product_display_page_image-376x470.jpg",
                                                                "web_image": "https://dpnexo4f8mouj.cloudfront.net/cache/product_display_page_image-376x470.jpg",
                                                                "store_type": "",
                                                                "domain": "",
                                                                "is_styfi_fulfillable":0,
                                                                "store_id": 0,
                                                                "has_sheet":1,
                                                                "user_group_id": 1,
                                                                "brand_id": null,
                                                                "is_enable": 1,
                                                                "is_admin": true,
                                                                "auth_token":adminData[0].token,
                                                                "activate_account" :sails.config.styfi.activate_account
                                                                }];
                                                                return  cb(false, Data);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                } else {
                                    // redirect URL to SignUp page.
                                    return  cb({
                                        'error' :true,
                                        'code' :404,
                                        'message' :'please go to SignUp page. Thank you!'
                                      });
                                }
                            });
                }
                else {
                    return  cb({
                        'error' :true,
                        'code' :404,
                        'message' :'mobile_number does not exists!'
                      });
                }
            });
        }
        else {
            return cb({
                "code": 403,
                "message": "Required Parameters Missing"
            });
        }
    },


    identification:function (mobile_number,cb) {
        User.query('SELECT * FROM sl_customer WHERE mobile_number ='+mobile_number, function (err, resp) {
            if (resp.length > 0){

                if (resp[0].is_mobile_verified == 1)
                {
                    resp[0].is_mobile_verified =true;
                }else {
                    resp[0].is_mobile_verified=false;
                }


                if (resp[0].is_email_verified == 1)
                {
                    resp[0].is_email_verified =true;
                }else {
                    resp[0].is_email_verified=false;
                }
                return cb({
                    'error':true,
                    'code':200,
                    'message':'success',
                    'data':{
                        'username':resp[0].username,
                        'email':resp[0].email,
                        'mobile_number':resp[0].mobile_number,
                        'is_email_verified':resp[0].is_email_verified,
                        'is_mobile_verified':resp[0].is_mobile_verified
                    }
                });
            }else {
                return  cb({
                    'error' :true,
                    'code' :404,
                    'message' :'mobile_number does not exists!'
                });
            }
    });
    },

    resend_otp:function (mobile_number,cb) {
        if (mobile_number)
        {
            var digits = sails.config.styfi.SMS_CODE_DIGIT;
            var code = Math.floor(Math.pow(10, digits - 1) + Math.random() * (Math.pow(10, digits) - Math.pow(10, digits - 1) - 1));
            query6 = 'INSERT INTO sl_mobile_otp_details SET mobile_number = "' + mobile_number + '", code = "' + code + '", send_count = 1, check_count = 0, otp_expire = DATE_ADD(now(), INTERVAL 15 MINUTE), status = 1, date_added = now(), date_updated=now()';
            User.query(query6, function (err, resp) {
                var otp_id = resp.insertId;
                if (otp_id) {
                    var message = code + " is the OTP to verify your phone number. This OTP will be usable only once and is valid for " + sails.config.styfi.SMS_OTP_EXPIRE_TIME + " minutes from the request.";
                    sms.send_sms(otp_id, mobile_number, message, function (err, resp1) {
                        if (err) {
                            return cb(err);
                        }
                        else {
                            //console.log(resp8);
                            if (resp1.status == 'failure') {
                                return  cb({
                                    'error' :true,
                                    'code' :404,
                                    'message' :'Internal Server Error.'
                                });
                            }
                            else {
                                return  cb(false);
                            }
                        }
                    });
                    //console.log(smsResponse);

                }
                else {
                    return  cb({
                        'error' :true,
                        'code' :404,
                        'message' :'Internal Server Error.'
                    });
                }
            });
        }
        else
        {
            return  cb({
                "error":true,
                "code": 403,
                "message": "Required Parameters Missing"
            });
        }
    },


  send_otp: function (mobile_number, cb) {
      if (mobile_number) {
          User.query('SELECT * FROM sl_customer WHERE mobile_number ="' + mobile_number + '"', function (err, resp1) {
              if (err) {
                  return cb(err);
              }
              //console.log(resp1)
              if (resp1.length == 1) {
                  var customer_id = resp1[0].customer_id;
                  //console.log(customer_id);
                  query1 = 'select * from sl_mobile_otp_details where customer_id = "' + customer_id + '" and mobile_number="' + mobile_number + '" and status in (1,2) and now() < otp_expire';
                  User.query(query1, function (err, resp2) {
                      //console.log(resp2);
                      if (err) {
                          return cb(err);
                      }
                      if (resp2.length == 1) {
                          if (resp2[0].status == 1 && resp2[0].send_count + 1 <= sails.config.styfi.SMS_MAX_OTP_SEND_LIMIT) {
                              query4 = 'UPDATE sl_mobile_otp_details SET send_count = send_count+1, check_count = 0, otp_expire = DATE_ADD(now(), INTERVAL "' + sails.config.styfi.SMS_OTP_EXPIRE_TIME + '" MINUTE), date_updated=now() WHERE otp_id = "' + resp2[0].otp_id + '"';
                              var message = resp2[0].code + " is the OTP to verify your phone number. This OTP will be usable only once and is valid for " + sails.config.styfi.SMS_OTP_EXPIRE_TIME + " minutes from the request.";
                              User.query(query4, function (err, resp4) {
                                  if (err) {
                                      return cb(err);
                                  }
                                  else {
                                      sms.send_sms(resp2[0].code, mobile_number, message, function (err, resp7) {

                                          if (err) {
                                              return  cb(err);
                                          }
                                          else {
                                              //console.log(resp7);
                                              if (resp7.status == 'failure') {
                                                  return  cb({
                                                      'error' :true,
                                                      'code' :404,
                                                      'message' :'Internal Server Error.'
                                                    });
                                              }
                                              else {
                                                  return  cb(false);
                                              }
                                          }
                                      });
                                  }
                              });
                          }
                          else {
                              if (resp2[0].status == 2) {
                                  return cb({
                                      'code' :203,
                                      'error' :true,
                                      'message' :'Your mobile number has been blocked for 15 mins. You may try again with another number.'
                                    });
                              }
                              else {
                                  query2 = 'UPDATE sl_mobile_otp_details SET mobile_expire = DATE_ADD(now(), INTERVAL"' + sails.config.styfi.SMS_MOBILE_EXPIRE_TIME + '" MINUTE), status=2,date_updated=now() WHERE otp_id = "' + resp2[0].otp_id + '"';
                                  User.query(query2, function (err, resp3) {
                                      if (err) {
                                          return  cb(err);
                                      }
                                      else {
                                          return   cb({
                                              'code' :203,
                                              'error' :true,
                                              'message' :'We seem to be facing an internal error. Use a different mobile number, or try again later.'
                                            });
                                      }
                                  });
                              }
                          }
                      } else {
                          query5 = 'UPDATE sl_mobile_otp_details SET status = 0 WHERE customer_id = "' + customer_id + '"';
                          User.query(query5, function (err, resp5) {
                              if (err) {
                                  return cb(err);
                              }
                              else {
                                  var digits = sails.config.styfi.SMS_CODE_DIGIT;
                                  var code = Math.floor(Math.pow(10, digits - 1) + Math.random() * (Math.pow(10, digits) - Math.pow(10, digits - 1) - 1));
                                  query6 = 'INSERT INTO sl_mobile_otp_details SET customer_id= "' + customer_id + '" , mobile_number = "' + mobile_number + '", code = "' + code + '", send_count = 1, check_count = 0, otp_expire = DATE_ADD(now(), INTERVAL 15 MINUTE), status = 1, date_added = now(), date_updated=now()';
                                  User.query(query6, function (err, resp6) {
                                      var otp_id = resp6.insertId;
                                      if (otp_id) {
                                          var message = code + " is the OTP to verify your phone number. This OTP will be usable only once and is valid for " + sails.config.styfi.SMS_OTP_EXPIRE_TIME + " minutes from the request.";
                                          sms.send_sms(otp_id, mobile_number, message, function (err, resp8) {
                                              if (err) {
                                                  return cb(err);
                                              }
                                              else {
                                                  //console.log(resp8);
                                                  if (resp8.status == 'failure') {
                                                      return  cb({
                                                          'error' :true,
                                                          'code' :404,
                                                          'message' :'Internal Server Error.'
                                                        });
                                                  }
                                                  else {
                                                      return  cb(false);
                                                  }
                                              }
                                          });
                                          //console.log(smsResponse);

                                      }
                                      else {
                                          return  cb({
                                              'error' :true,
                                              'code' :404,
                                              'message' :'Internal Server Error.'
                                            });
                                      }
                                  });
                              }
                          });
                      }
                  });
              }
              else {
                  return  cb({
                      'error' :true,
                      'code' :404,
                      'message' :'mobile_number does not exists!'
                    });
              }
          });
      }
      else {
          return  cb({
              "error":true,
              "code": 403,
              "message": "Required Parameters Missing"
          });
      }
  },




  verify_otp: function (otp, cb) {
    if (otp) {
      query='SELECT * FROM sl_mobile_otp_details WHERE code="'+otp+'" and status in (1,2)';
      User.query(query, function(err, resp1){
        // console.log(resp1);
          if (err) {
              return cb(err);
          }else {
            if (resp1.length == 1) {
              if (resp1[0].code == otp) {
                if (resp1[0].is_expire ) {
                    return  cb({
                        'error' :true,
                        'code'  : 201,
                        'message' :'Your OTP has expired. Hit resend for a different OTP.'
                  });
                }else {
                    query1='UPDATE sl_mobile_otp_details SET status=3 WHERE otp_id="'+resp1[0].otp_id+'" and customer_id="'+resp1[0].customer_id+'"';
                    User.query(query1, function (err, resp2){
                      if (err) {
                          return  cb(err);
                      }else{
                        query2='UPDATE  sl_customer SET is_mobile_verified=1 WHERE customer_id="'+resp1[0].customer_id+'"';
                        User.query(query2, function(err, resp3){
                          if (err) {
                              return  cb(err);
                          }else{
                              return cb(false);
                          }
                        });
                      }
                    });
                }
              }else {
                if (resp1[0].send_count < sails.config.styfi.SMS_MAX_OTP_SEND_LIMIT) {
                  query5='UPDATE sl_mobile_otp_details SET check_count=check_count+1, date_updated=now() WHERE otp_id="'+resp1[0].otp_id+'"';
                  User.query(query5, function (err,resp6){
                    if (err) {
                        return cb(err);
                    }
                      return  cb({
                          'error' :true,
                          'code'  : 202,
                          'message' :'You seem to have entered an incorrect or expired OTP. Hit resend for a new one.'
                        });

                  });
                }else {
                  query6='UPDATE sl_mobile_otp_details SET mobile_expire = DATE_ADD(now(), INTERVAL "'+sails.config.styfi.SMS_MOBILE_EXPIRE_TIME +'" MINUTE), status=2, date_updated=now() WHERE otp_id="'+resp1[0].otp_id+'"';
                  User.query(query6, function(err,resp7){
                    if (err) {
                        return cb(err);
                    }else {
                        return cb({
                          'error' :true,
                          'code'  : 203,
                          'message' :'Your mobile number has been blocked for 15 mins. You may try again with another number.'
                        });
                    }
                  });
                }
              }
            }else {
                return  cb({
                    'error' :true,
                    'code'  : 202,
                    'message' :'You seem to have entered an incorrect or expired OTP. Hit resend for a new one.'
              });
            }
          }
      });
    }else {
        return  cb({
              "code": 403,
              "message": "Kindly enter your OTP before clicking on submit"
          });
    }
  },

    regenerate: function (email, callback) {

      var query="SELECT * FROM sl_customer where email='"+email+"'";
        User.query(query , function (err, responseData){
            if (err){
                return callback(err);
            }else {
                var obj = {"email":responseData[0].email,"timestamp":new Date()};

                var buf = new Buffer (JSON.stringify(obj));

                //return callback(responseData);
                var data ={
                    customer_id: responseData[0].customer_id,
                    foreign_id: responseData[0].foreign_id,
                    type: responseData[0].type,
                    username: responseData[0].username,
                    mobile_number: responseData[0].mobile_number,
                    email: responseData[0].email,
                    unique_mob_number: responseData[0].unique_mob_number,
                    salt: responseData[0].salt,
                    date_added: responseData[0].data_added,
                    status: responseData[0].status,
                    is_email_verified: responseData[0].is_email_verified,
                    is_mobile_verified: responseData[0].is_mobile_verified,
                    activate_account:sails.config.styfi.activate_account +"?key="+buf
                };

                sails.renderView('welcome',data, function (err, emailContent) {
                    if (err) {
                        return callback(err);
                    } else {
                        const KEY = sails.config.styfi.EMAIL_KEY;
                        var helper = require('sendgrid').mail;
                        var fromEmail = new helper.Email(sails.config.styfi.email_from);
                        var toEmail = new helper.Email(responseData[0].email);
                        var subject = "STYFI - Welcome";
                        var content = new helper.Content(sails.config.styfi.mimeType, emailContent);
                        var mail = new helper.Mail(fromEmail, subject, toEmail, content);
                        mail.personalizations[0].addCc(new helper.Email('karan@stylabs.in'));
                        var sg = require('sendgrid')(KEY);
                        var request = sg.emptyRequest({
                            method: 'POST',
                            path: '/v3/mail/send',
                            body: mail.toJSON()
                        });
                        sg.API(request, function (error, response) {

                            if (error) {
                                return callback(err);
                            }else {
                                //console.log(response);
                                return callback(false);
                            }
                        });
                    }
                });

            }
        });
    },

    newToken:function (token,user_id,cb){
      var query1 ='SELECT * FROM sl_admin_api_token where user_id='+user_id+' and token="'+token+'"';
      User.query(query1,function(err,resp){
          if(resp.length === 1){
              return cb(null, {
                  'code': 200,
                  'message': 'success',
                  "token":token
              });
              //var salt = "";
              //var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
              //for (var i = 0; i < 5; i++)
               //   salt += possible.charAt(Math.floor(Math.random() * possible.length));
              //var new_token = sails.md5(salt+resp[0].token);
              //var query2= 'UPDATE sl_admin_api_token SET token="'+new_token+'" WHERE admin_api_id='+resp[0].admin_api_id+'';
              // User.query(query2, function (err, resp1){
              //     if(resp1.affectedRows === 1 ){
              //         return cb(null, {
              //             'code': 200,
              //             'message': 'success',
              //             "token":token
              //
              //         });
              //     }else {
              //         return  cb(null,{
              //             "code":500,
              //             "message": "new token not available!",
              //             "token":""
              //         });
              //     }
              // });
          }else {
              return  cb(null,{
                  "code":403,
                  "message": "You are not permitted to perform this action.!",
                  "token":""
              });
          }
      });
    },

    logout: function(auth_token,type,cb) {
      if (type == 'user'){
          User.query('UPDATE sl_admin_api_token SET status=0 WHERE token="'+auth_token+'"',function(err, rawRes){
              //console.log("11111",rawRes);
              if(err){
                  return  cb(err);
              }else{
                  return  cb(false);
              }
          });
      }
      else
      {
          User.query('UPDATE sl_api_auth_token SET logout_status=0 WHERE token="'+auth_token+'"',function(err, rawRes1){
              //console.log('2222',rawRes1);
              if(err){
                  return  cb(err);
              }else{
                  return cb(false);
              }
          });
      }


  },



  check_auth_token:function(auth_token,type,cb){
    if (auth_token && type) {
      var checkSQL;
      if(type == 'user'){
        checkSQL ="select sat.* from "+db_sl_prefix+"admin_api_token sat , "+db_sl_prefix+"customer ou where sat.token='"+auth_token+"' and sat.`status`=1 and expires > NOW() and ou.foreign_id= sat.user_id";
      }else{
        checkSQL ="select sat.* from "+db_sl_prefix+"api_auth_token sat , "+db_sl_prefix+"customer ou where sat.token='"+auth_token+"' and sat.`status`=1 and expires > NOW() and ou.foreign_id= sat.customer_id";
      }
      User.query(checkSQL,function(error, output){
        if(error){
            return  cb(error);
        }else
        {
          if(output.length==1)
          {
              return  cb(false,output);
          }else
          {
              return cb({
                "code":"401",
                "message": "Authentication failed"
              });
          }
        }
      });
    }else
    {
        return  cb({
        "code":"403",
        "message": "Required Parameters Missing"
      });
    }
  }



};
