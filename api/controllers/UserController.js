/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /**
     * @api {POST} /login User login
     * @apiName login
     * @apiGroup User
     * @apiParam {Number} mobile_number
     * @apiSuccessExample {json} Success-Response:
     {
         "code": 200,
         "message": "No Error",
         "data": {
             "type": "user",
             "user_id": 26,
             "name": "london-bee",
             "store_type": "shopify",
             "domain": "london-bee.myshopify.com",
             "store_id": 54,
             "brand_id": 704,
             "is_enable": 1
         },
         "token": "69d7224178ca89a36ee83d1bbd491b19"
     }
     * @apiErrorExample Error-1:
     * {"code":403,"message":"Required Data Missing"}
     * @apiErrorExample Error-2:
     * {"code":401,"message":"Authentication Failed"}
     */
	  login: function (req, res)
	  {
	   //console.log(req.param('mobile_number'));
	   return res.login({
	     mobile_number: req.param('mobile_number'),
	     successRedirect: '/',
	     invalidRedirect: '/login'
	   });
	 },

    validate: function (req,res){
	      var token= req.param('token');
        var user_id= req.param('user_id');
	      if(token && user_id){
	          User.newToken(token,user_id, function (err,response){
	              if(err){
                      return res.badRequest(err);
                  }else {
                      return res.ok(response);
                  }
              });
          }else {
              return  res.ok({
                  "code": 403,
                  "message": "required parameter missing"
              });
          }
    },



    regenerate: function (req, res) {
        var email= req.param('email');
        if(email)
        {
            User.regenerate(email, function (err, response) {
                if (err){
                    return res.badRequest(err);
                }else
                {
                    return res.ok({
                        "code": "200",
                        "message": "Regenerate Link Send Successfully!"
                    });
                }
            });
        }else {
            return  res.ok({
                "error":true,
                "code": 403,
                "message": "Required Parameters Missing"
            });
        }

    },


	 signup: function (req, res)
	 {
		 return res.signup({
			 username: req.param('username'),
			 email: req.param('email'),
			 mobile_number: req.param('mobile_number'),
			 type: req.param('type'),
             foreign_id:req.param('foreign_id'),
			 successRedirect: '/',
			 invalidRedirect: '/signup'
		 });
	 },

	indentify: function (req, res) {
		var mobile_number= req.param('mobile_number');
		if (mobile_number){
            User.identification(mobile_number,function (err,data) {
                if (err)
                {
                    return res.badRequest(err);
                }else {
                    return res.ok(data);
                }
            });
		}else {
            return  res.ok({
                "error":true,
                "code": 403,
                "message": "Required Parameters Missing"
            });
		}

    },

/**
 * @api {POST} /send_otp Send OTP
 * @apiName send_otp
 * @apiGroup User
 * @apiParam {Number} mobile_number
 * @apiSuccessExample {json} Success-Response:
 {
     "code": 200,
     "message": "success",
     "data": {
     "regex": "^\\\\d{4}",
         "sms_from": "XSTYFI"
 }
 }
 */

	 send: function (req, res){
		 var mobile_number = req.param('mobile_number');
		 if (mobile_number){
             User.send_otp(mobile_number,function(err){
                 if (err)
                 {
                     return res.badRequest(err);
                 }
                 else
                 {
                     return res.ok({
                         'code' : 200,
                         'message':'success',
                         'data':{
                             'regex':'^\\\\d{4}',
                             'sms_from':sails.config.styfi.SMS_SENDER
                         }
                     });
                 }
             });
		 }else {
             return  res.ok({
                 "error":true,
                 "code": 403,
                 "message": "Required Parameters Missing"
             });
		 }
	 },

    /**
     * @api {POST} /resend_otp Resend OTP
     * @apiName resend_otp
     * @apiGroup User
     * @apiParam {Number} mobile_number
     * @apiSuccessExample {json} Success-Response:
     {
         "code": 200,
         "message": "success",
         "data": {
         "regex": "^\\\\d{4}",
             "sms_from": "XSTYFI"
     }
     }
     */
    resend: function (req, res){
        var mobile_number = req.param('mobile_number');
        if (mobile_number){
            User.resend_otp(mobile_number,function(err){
                if (err)
                {
                    return res.badRequest(err);
                }
                else
                {
                    return res.ok({
                        'code' : 200,
                        'message':'success',
                        'data':{
                            'regex':'^\\\\d{4}',
                            'sms_from':sails.config.styfi.SMS_SENDER
                        }
                    });
                }
            });
        }else {
            return  cb({
                "error":true,
                "code": 403,
                "message": "Required Parameters Missing"
            });
        }
    },
    /**
     * @api {POST} /verify_otp verify OTP
     * @apiName verify_otp
     * @apiGroup User
     * @apiParam {Number} otp
     * @apiSuccessExample {json} Success-Response:
     {
         "error": false,
         "code": 200,
         "message": "success"
     }
     */
    verify: function (req, res){
		 var otp = req.param('otp');
		 if (otp){
             User.verify_otp(otp,function (err){
                 if (err)
                 {
                     return res.badRequest(err);
                 }
                 else
                 {
                     return res.ok({
                         'error':false,
                         'code' : 200,
                         'message':'success'
                     });
                 }
             });
		 }else {
             return  cb({
                 "error":true,
                 "code": 403,
                 "message": "Required Parameters Missing"
             });
		 }
	 },
    /**
     * @api {POST} /logout User logout
     * @apiName logout
     * @apiGroup User
     * @apiParam {String} auth_token
     * @apiParam {String} type
     * @apiSuccessExample {json} Success-Response:
     *
     {
       "message": "success",
       "data":
           {
              "code": "200",
              "message": "Logged out successfully!"
           }
     }
     */
    logout: function (req, res) {
    req.session.me = null;
    req.session.authenticated = false;
    var auth_token = req.headers['authorization'].split(' ');
    var type =req.param('type');
    if(auth_token && type)
    {
        User.logout(auth_token[1],type,function(err){
            if(err){
                return res.badRequest(err);
            }
            if (req.wantsJSON) {
                return res.ok({
                    "code": "200",
                    "message": "Logged out successfully!"
                });
            }
            return res.redirect('/');
        });
    }
    else
    {
        return  res.ok({
            "error":true,
            "code": 403,
            "message": "Required Parameters Missing"
        });
    }

  }

};
