/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {
    if (req.headers['authorization']) {
        //console.log("33333",req.headers['authorization']);
        var auth_token = req.headers['authorization'].split(' ');
        var authQuery;
        authQuery = "SELECT customer_id FROM sl_api_auth_token where token='"+auth_token[1]+"' AND logout_status = 1 AND expire_time > NOW()\n" +
            "UNION \n" +
            "SELECT user_id FROM sl_admin_api_token where token='"+auth_token[1]+"' AND status = 1 AND expires > NOW()";
        //console.log(authQuery);
        User.query(authQuery,function(err, rawRes){
            if(err) return res.forbidden(err);
            //console.log(rawRes);
            if(rawRes.length==1){
                return next();
            }else{
                return res.forbidden({
                    "code":"401",
                    "message": "You are not permitted to perform this action."
                });
            }
        });
    }
    else
    {
        return res.forbidden({
            "code":"403",
            "message": "Invalid Request!",
        });
    }
};