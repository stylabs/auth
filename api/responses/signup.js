/**
 * res.login([inputs])
 *
 * @param {String} inputs.username
 * @param {String} inputs.password
 *
 * @description :: Log the requesting user in using a passport strategy
 * @help        :: See http://links.sailsjs.org/docs/responses
 */
var request = require('request');
module.exports = function signup(inputs) {
    inputs = inputs || {};

    // Get access to `req` and `res`
    var req = this.req;
    var res = this.res;

    // Look up the user
    User.attemptsignup({
        username: inputs.username,
        email: inputs.email,
        mobile_number:inputs.mobile_number,
        type:inputs.type,
        foreign_id:inputs.foreign_id
    }, function (err, user) {
        if (err) return res.negotiate(err);
        if (!user) {
            req.session.authenticated = false;
            if (req.wantsJSON || !inputs.invalidRedirect) {
                return res.badRequest('Invalid username/email/mobile_number combination.');
            }
            return res.redirect(inputs.invalidRedirect);
        }
        //console.log(user);
        if(user[0]){

            req.session.authenticated = true;
            if (user[0].logout_status === 0)
            {
                user[0].logout_status = true
            }
            else
            {
                user[0].logout_status = false
            }

            // SERIALIZE PARAMETER

     //        var doc = { 'time' : timestamp };
     //        var data1 = bson.serialize(doc);
     //        console.log(data1,"data1");
     //        //data1 = JSON.stringify(data1);
     //        //var abc =JSON.parse(data1);
     //
     //        var doc1 = { 'email' : 'pravin@gmail.com' };
     //        var email = bson.serialize(doc1);
     //       email = JSON.stringify(email);
     //        console.log(email,"email");
     // //       var mailid =JSON.parse(email);

            var obj = {"email":user[0].email,"timestamp":new Date()};

            var buf = new Buffer (JSON.stringify(obj));



            var data ={
                customer_id: user[0].customer_id,
                foreign_id: user[0].foreign_id,
                type: user[0].type,
                username: user[0].username,
                mobile_number: user[0].mobile_number,
                email: user[0].email,
                unique_mob_number: user[0].unique_mob_number,
                salt: user[0].salt,
                date_added: user[0].data_added,
                status: user[0].status,
                is_email_verified: user[0].is_email_verified,
                is_mobile_verified: user[0].is_mobile_verified,
                api_token_id: user[0].api_token_id,
                token: user[0].auth_token,
                device_type: user[0].device_type,
                create_datetime: user[0].create_datetime,
                expire_time: user[0].expire_time,
                logout_status: user[0].logout_status,
                activate_account:sails.config.styfi.activate_account +"?key="+buf
            };

            // EMAIL SEND

            sails.renderView('welcome',data, function (err, emailContent) {
                if (err) {
                    res.negotiate(err);
                } else {
                    const KEY = sails.config.styfi.EMAIL_KEY;
                    var helper = require('sendgrid').mail;
                    var fromEmail = new helper.Email(sails.config.styfi.email_from);
                    var toEmail = new helper.Email(user[0].email);
                    var subject = "STYFI - Welcome";
                    var content = new helper.Content(sails.config.styfi.mimeType, emailContent);
                    var mail = new helper.Mail(fromEmail, subject, toEmail, content);

                    var sg = require('sendgrid')(KEY);
                    var request = sg.emptyRequest({
                        method: 'POST',
                        path: '/v3/mail/send',
                        body: mail.toJSON()
                    });
                    sg.API(request, function (error, response) {

                        if (error) {
                            res.negotiate(error);
                        }else {
                            //console.log(response);
                        }
                    });
                }
            });

            // RESPONSE

            if (req.wantsJSON || !inputs.successRedirect) {

                return res.ok({
                    "code": 200,
                    "message": "No Error",
                    "data": {
                        "foreign_id":user[0].foreign_id,
                        "type":user[0].type,
                        "username":user[0].username,
                        "email":user[0].email,
                        "mobile_number":user[0].mobile_number,
                        "data_added":user[0].data_added,
                        "logged_in":user[0].logout_status,
                        "activate_account" :sails.config.styfi.activate_account +"?key="+buf
                    },
                    "auth_token":user[0].token
                });
            }
            //return res.redirect(inputs.successRedirect);
        }else{
            req.session.authenticated = false;
            res.json(401, { error: 'Invalid username/email/mobile_number combination.' });
        }
    });

};
