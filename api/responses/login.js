
/**
 * res.login([inputs])
 *
 * @param {String} inputs.username
 * @param {String} inputs.password
 *
 * @description :: Log the requesting user in using a passport strategy
 * @help        :: See http://links.sailsjs.org/docs/responses
 */

module.exports = function login(inputs) {
  inputs = inputs || {};

  // Get access to `req` and `res`
  var req = this.req;
  var res = this.res;

  // Look up the user
  User.attemptLogin({
    mobile_number: inputs.mobile_number
  }, function (err, user) {
    if (err) return res.negotiate(err);
    if (!user) {
      req.session.authenticated = false;
      if (req.wantsJSON || !inputs.invalidRedirect) {
        return res.badRequest('Invalid mobile_number combination.1');
      }
      return res.redirect(inputs.invalidRedirect);
    }
    //console.log(user);
    if(user[0]){
      req.session.authenticated = true;
        var is_admin = false;
      if (user[0].user_group_id == 1)
      {
          user[0].store_id = user[0].store_id;
          is_admin = true;
      }

      if (req.wantsJSON || !inputs.successRedirect) {
        if (user[0].type == 'customer')
        {
            return res.ok({
                "code": 200,
                "message": "No Error",
                "data": {
                    "type":user[0].type,
                    "customer_id":user[0].customer_id,
                    "username":user[0].firstname +" "+ user[0].lastname,
                    "email":user[0].email,
                    "is_styfi_fulfillable":0,
                    "mobile_number":user[0].mobile_number,
                    "logged_in":user[0].logout_status,
                    "verified":user[0].is_verified
                },
                "auth_token":user[0].token
            });
        }else if (user[0].type == 'user')
        {
          return res.ok({
              "code": 200,
              "message": "No Error",
              "data": {
                    "type":user[0].type,
                    "user_id":user[0].user_id,
                    "name":user[0].name,
                    "brand_name":user[0].brand_name,
                    "brand_image":user[0].brand_image,
                    "cover_image":user[0].cover_image,
                    "web_image":user[0].web_image,
                    "background_image":user[0].background_image,
                    "sign_image":user[0].sign_image,
                    "store_type":user[0].store_type,
                    "has_sheet":user[0].has_sheet,
                    "domain":user[0].domain,
                    "store_id":user[0].store_id,
                    "is_styfi_fulfillable":user[0].is_styfi_fulfillable,
                    "user_group_id":user[0].user_group_id,
                    "brand_id":user[0].brand_id,
                    "is_enable":user[0].is_enable,
                    "is_admin":is_admin
                    },
                    "token":user[0].token
          });
        } else
        {
            return res.ok({
                "code": 200,
                "message": "No Error",
                "data":{
                    "type": "admin",
                    "user_id": user[0].user_id,
                    "name": 'styfi',
                    "brand_name": 'STYFI',
                    "brand_image": "https://s3-ap-southeast-1.amazonaws.com/styfi-image/cache/catalog/manufacturers/S-200x200.png",
                    "cover_image": "https://dpnexo4f8mouj.cloudfront.net/cache/product_display_page_image-376x470.jpg",
                    "web_image": "https://dpnexo4f8mouj.cloudfront.net/cache/product_display_page_image-376x470.jpg",
                    "store_type": "",
                    "is_styfi_fulfillable":0,
                    "has_sheet":user[0].has_sheet,
                    "domain": "",
                    "store_id": 0,
                    "user_group_id": 1,
                    "brand_id": null,
                    "is_enable": 1,
                    "is_admin": true
                },
                "token":user[0].auth_token
            });
        }

      }
      return res.redirect(inputs.successRedirect);
    }else{
      req.session.authenticated = false;
      res.json(401, { error: 'Invalid mobile_number combination.2' });
    }
  });

};
