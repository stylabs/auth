module.exports.styfi = {
	db_sl_prefix:'sl_',
	db_prefix:'oc_',
	elastic_prefix_1:'styfi_',
	elastic_prefix:'sl_',
	cdn_url:'https://dpnexo4f8mouj.cloudfront.net/',
    s3_url:"https://s3-ap-southeast-1.amazonaws.com/styfi-image/cache/catalog/manufacturers/",
    cdn_blank_url:'https://dpnexo4f8mouj.cloudfront.net/cache/product_display_page_image-376x470.jpg',
	elastic_url:'https://search-styfi-a43wachlj7ilooaogmsnriynqi.ap-south-1.es.amazonaws.com:443',
	styfi_api: 'https://beta.styfi.in/',
    jwtSecret: 'some secret',

    //SMS Congfig
    SMS_SENDER: 'XSTYFI',
    SMS_OTP_REGEX: '/^\\d{4}/g',
    SMS_USERNAME: "info@stylabs.in",
    SMS_HASH_CODE: 'c6708763e129d6f35facdc11c3740ea682d243c9',
    SMS_URL:'http://api.textlocal.in/send/?',
    SMS_CODE_DIGIT: '4',
    SMS_OTP_EXPIRE_TIME:'3',
    SMS_MOBILE_EXPIRE_TIME: '15',
    SMS_MAX_OTP_SEND_LIMIT:'5',

    ccEmails:[
	    'pravin@stylabs.in',
        'sanket@stylabs.in'
        ],
    activate_account:'http://dash.styfi.in/onboard',
    EMAIL_KEY:'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',
    email_from:'hello@styfi.in',
    mimeType:'text/html',
    contentType:'html'
};

